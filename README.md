
## 주제 : 화면 설계서 기반 API 개발

<p align="justify">
주어진 화면 설계서 (https://xd.adobe.com/view/5d0ce398-81c3-4a0c-9cc0-2f1ff98bf0e5-085f/)를 참고하여 요구사항 충족하기
</p>


<br>

## 사용 기술 / 도구
* spring boot
* h2
* mybatis
* validation
* lombok
* postman

## 정리

## 패키지 구조
<p align="center">
  <br>
  <img src="./images/packageStructure.png">
  <br>
</p>

### DB 설계
<p align="center">
  <br>
  <img src="./images/dbImg.png">
  <br>
</p>

### API 정리

아티스트

| method | url | 설명 |
| --- | --- | --- |
| post | /artist | 아티스트 등록 |
| get | /artist/search | 아티스트 목록 조회 및 검색 |
| get | /artist/{id} | 아티스트 상세(단건) 조회 |
| put | /artist/{id} | 아티스트 수정 |
| delete | /artist/{id} | 아티스트 삭제 |

앨범

| method | url | 설명 |
| --- | --- | --- |
| post | /album | 앨범 등록 |
| get | /album/search | 앨범 목록 조회 및 검색 |
| get | /album/{id} | 앨범 상세(단건) 조회 |
| put | /album/{id} | 앨범 수정 |
| delete | /album/{id} | 앨범 삭제 |

<br>


