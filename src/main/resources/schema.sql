create table Artist(
    id              bigint        auto_increment,
    name            varchar(20)   not null ,
    birthday        timestamp     not null ,
    nation          varchar(10)   not null ,
    agency          varchar(20) ,
    description     varchar(1000) ,
    writer          varchar(20) ,
    createdDateTime timestamp ,
    primary key (id)
);

create table Album(
    id              bigint        auto_increment,
    artistId        bigint        not null ,
    name            varchar(20)   not null ,
    releaseDate     timestamp     not null ,
    genre           varchar(10) ,
    description     varchar(1000),
    writer          varchar(20) ,
    createdDateTime timestamp ,
    primary key (id),
    foreign key (artistId) references Artist(id)
);


create table SoundSource(
    id              bigint        auto_increment,
    albumId         bigint        not null ,
    name            varchar(20)   not null ,
    playTime        timestamp     not null ,
    exposure        boolean       not null ,
    musicOrder      int           not null ,
    primary key (id),
    foreign key (albumId) references Album(id)
);
