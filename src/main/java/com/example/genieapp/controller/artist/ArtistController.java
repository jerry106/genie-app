package com.example.genieapp.controller.artist;


import com.example.genieapp.common.paging.PagingRequest;
import com.example.genieapp.common.paging.PagingResponse;
import com.example.genieapp.dto.request.artist.ArtistFormRequest;
import com.example.genieapp.dto.response.artist.ArtistFormResponse;
import com.example.genieapp.service.artist.ArtistService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/artist")
@RequiredArgsConstructor
public class ArtistController {

    private final ArtistService artistService;

    @PostMapping("")
    public void create(@Validated @RequestBody ArtistFormRequest artistFormRequest){

        artistService.create(artistFormRequest);
    }

    @GetMapping("/search")
    public PagingResponse<ArtistFormResponse> search(@RequestParam(defaultValue = "") String searchWord, @Validated PagingRequest pagingRequest){

        return artistService.search(searchWord, pagingRequest);
    }

    @GetMapping("/{id}")
    public ArtistFormResponse read(@PathVariable Long id){

        return artistService.read(id);
    }

    @PutMapping("/{id}")
    public void modify(@PathVariable Long id ,@Validated @RequestBody ArtistFormRequest artistFormRequest){

        artistService.modify(id, artistFormRequest);
    }

    @DeleteMapping("/{id}")
    public void remove(@PathVariable Long id){

        artistService.remove(id);
    }
}
