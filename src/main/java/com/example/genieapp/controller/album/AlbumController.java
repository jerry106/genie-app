package com.example.genieapp.controller.album;

import com.example.genieapp.common.paging.PagingRequest;
import com.example.genieapp.common.paging.PagingResponse;
import com.example.genieapp.dto.request.album.AlbumFormRequest;
import com.example.genieapp.dto.response.album.AlbumFormResponse;
import com.example.genieapp.service.album.AlbumService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/album")
@RequiredArgsConstructor
public class AlbumController {

    private final AlbumService albumService;

    @PostMapping("/")
    public void create(@Validated @RequestBody AlbumFormRequest albumFormRequest){

        albumService.create(albumFormRequest);
     }

    @GetMapping("/search")
    public PagingResponse<AlbumFormResponse> search(@RequestParam(defaultValue = "") String searchWord, @Validated PagingRequest pagingRequest){

        return albumService.search(searchWord, pagingRequest);
    }

    @GetMapping("/{id}")
    public AlbumFormResponse read(@PathVariable Long id){

        return albumService.read(id);
    }

    @PutMapping("/{id}")
    public void modify(@PathVariable Long id,@Validated @RequestBody AlbumFormRequest albumFormRequest){

        albumService.modify(id, albumFormRequest);
    }

    @DeleteMapping("/{id}")
    public void remove(@PathVariable Long id){

        albumService.remove(id);
    }
}
