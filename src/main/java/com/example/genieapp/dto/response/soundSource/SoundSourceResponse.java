package com.example.genieapp.dto.response.soundSource;

import com.example.genieapp.entity.SoundSource;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SoundSourceResponse {

    private Long id;

    private String name;

    private LocalTime playTime;

    private Boolean exposure;

    private int musicOrder;

    private Long albumId;

    public static SoundSourceResponse from(SoundSource soundSource){

        return SoundSourceResponse.builder()
                .id(soundSource.getId())
                .name(soundSource.getName())
                .playTime(soundSource.getPlayTime())
                .exposure(soundSource.getExposure())
                .musicOrder(soundSource.getMusicOrder())
                .albumId(soundSource.getAlbumId())
                .build();
    }
}
