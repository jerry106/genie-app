package com.example.genieapp.dto.response.album;

import com.example.genieapp.entity.Album;
import com.example.genieapp.dto.response.soundSource.SoundSourceResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AlbumFormResponse {

    private Long id;

    private String name;

    private LocalDate releaseDate;

    private String genre;

    private String description;

    private String writer;

    private LocalDateTime createdDateTime;

    private Long artistId;

    private String artistName;

    private List<SoundSourceResponse> contents;

    public static AlbumFormResponse from(Album album, List<SoundSourceResponse> soundSources){

        return AlbumFormResponse.builder()
                .id(album.getId())
                .name(album.getName())
                .releaseDate(album.getReleaseDate())
                .genre(album.getGenre())
                .description(album.getDescription())
                .createdDateTime(album.getCreatedDateTime())
                .writer(album.getWriter())
                .artistId(album.getArtistId())
                .artistName(album.getArtistName())
                .contents(soundSources)
                .build();
    }

    public static AlbumFormResponse from(Album album){

        return AlbumFormResponse.builder()
                .id(album.getId())
                .name(album.getName())
                .releaseDate(album.getReleaseDate())
                .genre(album.getGenre())
                .description(album.getDescription())
                .createdDateTime(album.getCreatedDateTime())
                .writer(album.getWriter())
                .artistId(album.getArtistId())
                .artistName(album.getArtistName())
                .build();
    }
}
