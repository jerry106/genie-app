package com.example.genieapp.dto.response.artist;

import com.example.genieapp.entity.Artist;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ArtistFormResponse {

    private Long id;

    private String name;

    private LocalDate birthday;

    private String agency;

    private String nation;

    private String description;

    private String writer;

    private LocalDateTime createdDateTime;

    public static ArtistFormResponse from(Artist artist){

        return ArtistFormResponse.builder()
                .id(artist.getId())
                .name(artist.getName())
                .birthday(artist.getBirthday())
                .agency(artist.getAgency())
                .nation(artist.getNation())
                .description(artist.getDescription())
                .writer(artist.getWriter())
                .createdDateTime(artist.getCreatedDateTime())
                .build();
    }
}
