package com.example.genieapp.dto.request.SoundSource;

import com.example.genieapp.entity.SoundSource;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SoundSourceRequest {

    @Size(max = 20, message = "20자를 초과할 수 없습니다.")
    @NotBlank(message = "음원명은 null, 공백을 허용하지 않습니다.는")
    private String name;

    @NotNull(message = "재생시간은 필수값입니다.")
    private LocalTime playTime;

    @NotNull(message = "노출 여부는 필수값입니다.")
    private Boolean exposure;

    @Positive(message = "순서는 양수값만 가능합니다.")
    @NotNull(message = "순서는 필수값입니다.")
    private int musicOrder;

    public SoundSource toEntity() {

        return SoundSource.builder()
                .name(getName())
                .playTime(getPlayTime())
                .exposure(getExposure())
                .musicOrder(getMusicOrder())
                .build();
    }
}
