package com.example.genieapp.dto.request.album;

import com.example.genieapp.dto.request.SoundSource.SoundSourceRequest;
import com.example.genieapp.entity.Album;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AlbumFormRequest {

    @NotBlank(message = "앨범명은 null, 공백을 허용하지 않습니다.")
    @Size(max = 20, message = "20자를 초과할 수 없습니다.")
    private String name;

    @NotNull(message = "발매일은 필수 값입니다.")
    private LocalDate releaseDate;

    @Size(max = 10, message = "10자를 초과할 수 없습니다.")
    private String genre;

    @Size(max = 1000, message = "1000자를 초과할 수 없습니다.")
    private String description;

    @NotBlank(message = "작성자는 null, 공백을 허용하지 않습니다.")
    @Size(max = 20, message = "20자를 초과할 수 없습니다.")
    private String writer;

    @NotNull(message = "아티스트 ID는 null을 허용하지 않습니다.")
    private Long artistId;

    @Valid
    private List<SoundSourceRequest> soundSources;

    public Album toEntity() {

        return Album.builder()
                .name(name)
                .releaseDate(releaseDate)
                .genre(genre)
                .description(description)
                .writer(writer)
                .createdDateTime(LocalDateTime.now())
                .artistId(artistId)
                .build();
    }
}
