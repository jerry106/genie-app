package com.example.genieapp.dto.request.artist;

import com.example.genieapp.entity.Artist;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArtistFormRequest {

    @NotBlank(message = "아티스트명은 null, 공백을 허용하지 않습니다.")
    @Size(max = 20, message = "20자를 초과할 수 없습니다.")
    private String name;

    @NotNull(message = "출생일은 필수 값입니다.")
    private LocalDate birthday;

    @Size(max = 20, message = "20자를 초과할 수 없습니다.")
    private String agency;

    @NotBlank(message = "국적은 null, 공백을 허용하지 않습니다.")
    @Size(max = 20, message = "20자를 초과할 수 없습니다.")
    private String nation;

    @Size(max = 1000, message = "1000자를 초과할 수 없습니다.")
    private String description;

    @NotBlank(message = "작성자 정보가 없습니다.")
    @Size(max = 20, message = "20자를 초과할 수 없습니다.")
    private String writer;

    public Artist toEntity(){
        return Artist.builder()
                .name(name)
                .agency(agency)
                .birthday(birthday)
                .description(description)
                .nation(nation)
                .writer(writer)
                .createdDateTime(LocalDateTime.now())
                .build();
    }
}
