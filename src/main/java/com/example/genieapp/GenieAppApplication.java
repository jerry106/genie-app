package com.example.genieapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenieAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(GenieAppApplication.class, args);
    }

}
