package com.example.genieapp.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SoundSource {

    private Long id;

    private String name;

    private LocalTime playTime;

    private Boolean exposure;

    private int musicOrder;

    private Long albumId;
}
