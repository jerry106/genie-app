package com.example.genieapp.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Album {

    private Long id;

    private String name;

    private LocalDate releaseDate;

    private String genre;

    private String description;

    private String writer;

    private LocalDateTime createdDateTime;

    private Long artistId;

    private String artistName;
}
