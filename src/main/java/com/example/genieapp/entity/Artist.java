package com.example.genieapp.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Artist {

    private Long id;

    private String name;

    private LocalDate birthday;

    private String agency;

    private String nation;

    private String description;

    private String writer;

    private LocalDateTime createdDateTime;
}
