package com.example.genieapp.repository.album;

import com.example.genieapp.common.paging.PagingRequest;
import com.example.genieapp.entity.Album;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AlbumRepository {

    Long createAndReturnAlbumId(Album album);

    int searchCount(String searchWord);

    List<Album> search(String searchWord, PagingRequest pagingRequest);

    void remove(Long id);

    void modify(Long id, Album album);

    Album read(Long id);

    int findAlbumCount(Long artistId);

    String findWriter(Long id);
}
