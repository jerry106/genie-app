package com.example.genieapp.repository.soundSource;

import com.example.genieapp.entity.SoundSource;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SoundSourceRepository {

    void create(SoundSource soundSource);

    List<SoundSource> read(Long id);

    void removeAll(Long albumId);
}
