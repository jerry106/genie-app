package com.example.genieapp.repository.artist;

import com.example.genieapp.common.paging.PagingRequest;
import com.example.genieapp.entity.Artist;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ArtistRepository {

    void create(Artist artist);

    int searchCount(String keyword);

    Artist read(Long id);

    void modify(Long id, Artist artist);

    void remove(Long id);

    List<Artist> search(String searchWord, PagingRequest pagingRequest);

    String findName(Long artistId);

    String findWriter(Long id);
}
