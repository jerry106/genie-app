package com.example.genieapp.common.exception;

import lombok.Builder;
import lombok.Getter;
import org.springframework.http.ResponseEntity;

@Getter
@Builder
public class ErrorResponse {

    private final int status;

    private final String error;

    private final String message;

    public static ResponseEntity<ErrorResponse> toResponse(ErrorCode errorCode){

        return ResponseEntity
                .status(errorCode.getHttpStatus())
                .body(ErrorResponse.builder()
                        .status(errorCode.getHttpStatus().value())
                        .error(errorCode.getHttpStatus().name())
                        .message(errorCode.getDetail())
                        .build());
    }
}
