package com.example.genieapp.common.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public enum ErrorCode {

    ARTIST_NOT_REMOVE(HttpStatus.BAD_REQUEST, "아티스트 정보가 앨범에 사용되고 있어 삭제할 수 없습니다."),
    SOUND_ORDER_DUPLICATE(HttpStatus.BAD_REQUEST, "음원 순서에 중복된 값이 포함되있습니다."),
    UNAUTHORIZED_WRITER(HttpStatus.UNAUTHORIZED, "작성자와 수정자의 정보가 일치하지 않습니다."),
    ARTIST_NOT_FOUND(HttpStatus.NOT_FOUND, "아티스트를 찾을 수 없습니다."),
    ALBUM_NOT_FOUND(HttpStatus.NOT_FOUND, "앨범을 찾을 수 없습니다."),
    ;

    private final HttpStatus httpStatus;

    private final String detail;
}
