package com.example.genieapp.common.paging;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class PagingResponse<T> {

    private int pageNo;

    private int pageSize;

    private int totalPageCount;

    private int totalDataCount;

    private List<T> contents;

    @Builder
    public PagingResponse(PagingRequest pagingRequest, int totalDataCount, List<T> contents) {
        this.pageNo = pagingRequest.getPageNo();
        this.pageSize = pagingRequest.getPageSize();
        this.totalDataCount = totalDataCount;
        this.contents = contents;
        this.totalPageCount = calculateTotalPageCount(pageSize, totalDataCount);
    }

    private int calculateTotalPageCount(int pageSize, int totalDataCount){
        if (totalDataCount % pageSize == 0){
            return totalDataCount / pageSize;
        }else {
            return totalDataCount / pageSize + 1;
        }
    }
}
