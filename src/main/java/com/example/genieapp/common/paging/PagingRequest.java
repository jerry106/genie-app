package com.example.genieapp.common.paging;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Data
@AllArgsConstructor
@Builder
public class PagingRequest {

    @PositiveOrZero(message = "0 또는 양수만 가능합니다.")
    private int pageNo;

    @Positive(message = "양수만 가능합니다.")
    private int pageSize;

    public PagingRequest(){
        this(0,5);
    }
}
