package com.example.genieapp.service.album;

import com.example.genieapp.common.paging.PagingRequest;
import com.example.genieapp.common.paging.PagingResponse;
import com.example.genieapp.dto.request.album.AlbumFormRequest;
import com.example.genieapp.dto.response.album.AlbumFormResponse;

public interface AlbumService {

    void create(AlbumFormRequest albumFormRequest);

    PagingResponse<AlbumFormResponse> search(String searchWord, PagingRequest pagingRequest);

    void modify(Long id, AlbumFormRequest albumFormRequest);
    void remove(Long id);

    AlbumFormResponse read(Long id);
}
