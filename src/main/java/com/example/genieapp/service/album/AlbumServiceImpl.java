package com.example.genieapp.service.album;

import com.example.genieapp.common.exception.CustomException;
import com.example.genieapp.common.paging.PagingRequest;
import com.example.genieapp.common.paging.PagingResponse;
import com.example.genieapp.entity.Album;
import com.example.genieapp.entity.SoundSource;
import com.example.genieapp.repository.album.AlbumRepository;
import com.example.genieapp.repository.artist.ArtistRepository;
import com.example.genieapp.repository.soundSource.SoundSourceRepository;
import com.example.genieapp.dto.request.SoundSource.SoundSourceRequest;
import com.example.genieapp.dto.request.album.AlbumFormRequest;
import com.example.genieapp.dto.response.album.AlbumFormResponse;
import com.example.genieapp.dto.response.soundSource.SoundSourceResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.example.genieapp.common.exception.ErrorCode.*;

@Service
@RequiredArgsConstructor
public class AlbumServiceImpl implements AlbumService{

    private final AlbumRepository albumRepository;
    private final SoundSourceRepository soundSourceRepository;
    private final ArtistRepository artistRepository;

    @Override
    @Transactional
    public void create(AlbumFormRequest albumFormRequest) {

        Long artistId = albumFormRequest.getArtistId();

        if (artistRepository.findName(artistId) == null){
            throw new CustomException(ARTIST_NOT_FOUND);
        }

        List<SoundSourceRequest> soundSources = albumFormRequest.getSoundSources();

        checkDuplicatedMusicOrder(soundSources);

        Album album = albumFormRequest.toEntity();

        albumRepository.createAndReturnAlbumId(album);

        Long albumId = album.getId();

        convertAndSaveSoundSource(albumId, soundSources);
    }

    @Override
    public AlbumFormResponse read(Long id) {

        Album album = albumRepository.read(id);

        if (album == null){
            throw new CustomException(ALBUM_NOT_FOUND);
        }

        List<SoundSource> soundSources = soundSourceRepository.read(id);

        List<SoundSourceResponse> convertedSoundSources = new ArrayList<>();

        for (SoundSource soundSource : soundSources){
            convertedSoundSources.add(SoundSourceResponse.from(soundSource));
        }

        return AlbumFormResponse.from(album, convertedSoundSources);
    }

    @Override
    public PagingResponse<AlbumFormResponse> search(String searchWord, PagingRequest pagingRequest) {

        int albumCount = albumRepository.searchCount(searchWord);

        List<Album> albums = albumRepository.search(searchWord, pagingRequest);

        List<AlbumFormResponse> convertedAlbums = new ArrayList<>();

        for (Album album : albums){
            convertedAlbums.add(AlbumFormResponse.from(album));
        }

        return new PagingResponse<>(pagingRequest, albumCount, convertedAlbums);
    }

    @Override
    @Transactional
    public void modify(Long id, AlbumFormRequest albumFormRequest) {

        if (albumRepository.read(id) == null){
            throw new CustomException(ALBUM_NOT_FOUND);
        }

        Long artistId = albumFormRequest.getArtistId();

        if (artistRepository.read(artistId) == null){
            throw new CustomException(ARTIST_NOT_FOUND);
        }

        if(!albumRepository.findWriter(id).equals(albumFormRequest.getWriter())){
            throw new CustomException(UNAUTHORIZED_WRITER);
        }

        List<SoundSourceRequest> soundSources = albumFormRequest.getSoundSources();

        checkDuplicatedMusicOrder(soundSources);

        Album album = albumFormRequest.toEntity();

        albumRepository.modify(id, album);

        soundSourceRepository.removeAll(id);

        convertAndSaveSoundSource(id, soundSources);
    }

    @Override
    @Transactional
    public void remove(Long id) {

        if (albumRepository.read(id) == null){
            throw new CustomException(ALBUM_NOT_FOUND);
        }

        soundSourceRepository.removeAll(id);

        albumRepository.remove(id);
    }

    private void convertAndSaveSoundSource(Long id, List<SoundSourceRequest> soundSources) {

        if (soundSources.size() != 0){
            for (int i = 0; i < soundSources.size(); i ++){
                SoundSource soundSource = soundSources.get(i).toEntity();
                soundSource.setAlbumId(id);
                soundSourceRepository.create(soundSource);
            }
        }
    }

    private void checkDuplicatedMusicOrder(List<SoundSourceRequest> soundSources){
        List<Integer> soundOrders = new ArrayList<>();

        for (SoundSourceRequest soundSource : soundSources) {
            soundOrders.add(soundSource.getMusicOrder());
        }

        Set<Integer> soundSet = new HashSet<>(soundOrders);

        if (soundSet.size() != soundOrders.size()){
            throw new CustomException(SOUND_ORDER_DUPLICATE);
        }
    }
}
