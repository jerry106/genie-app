package com.example.genieapp.service.artist;

import com.example.genieapp.common.exception.CustomException;
import com.example.genieapp.common.paging.PagingRequest;
import com.example.genieapp.common.paging.PagingResponse;
import com.example.genieapp.entity.Artist;
import com.example.genieapp.repository.album.AlbumRepository;
import com.example.genieapp.repository.artist.ArtistRepository;
import com.example.genieapp.dto.request.artist.ArtistFormRequest;
import com.example.genieapp.dto.response.artist.ArtistFormResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;

import static com.example.genieapp.common.exception.ErrorCode.*;

@Service
@RequiredArgsConstructor
public class ArtistServiceImpl implements ArtistService{

    private final ArtistRepository artistRepository;

    private final AlbumRepository albumRepository;

    @Override
    public void create(ArtistFormRequest artistFormRequest) {

        Artist artist = artistFormRequest.toEntity();

        artistRepository.create(artist);
    }

    @Override
    public ArtistFormResponse read(Long id) {

        Artist artist = artistRepository.read(id);

        if (artist == null){
            throw new CustomException(ARTIST_NOT_FOUND);
        }

        return ArtistFormResponse.from(artist);
    }

    @Override
    public void modify(Long id, ArtistFormRequest artistFormRequest) {

        if (artistRepository.read(id) == null) {
            throw new CustomException(ARTIST_NOT_FOUND);
        }

        if (!artistRepository.findWriter(id).equals(artistFormRequest.getWriter())){
            throw new CustomException(UNAUTHORIZED_WRITER);
        }

        Artist artist = artistFormRequest.toEntity();

        artistRepository.modify(id, artist);
    }

    @Override
    public void remove(Long id) {

        if (artistRepository.read(id) == null){
            throw new CustomException(ARTIST_NOT_FOUND);
        }

        if (albumRepository.findAlbumCount(id) != 0){
            throw new CustomException(ARTIST_NOT_REMOVE);
        }

        artistRepository.remove(id);
    }

    @Override
    public PagingResponse<ArtistFormResponse> search(String searchWord, PagingRequest pagingRequest) {

        int artistCount = artistRepository.searchCount(searchWord);

        List<Artist> artists = artistRepository.search(searchWord, pagingRequest);

        List<ArtistFormResponse> convertedArtists = new ArrayList<>();

        for (Artist artist : artists) {
            convertedArtists.add(ArtistFormResponse.from(artist));
        }

        return new PagingResponse<>(pagingRequest, artistCount, convertedArtists);
    }
}
