package com.example.genieapp.service.artist;

import com.example.genieapp.common.paging.PagingRequest;
import com.example.genieapp.common.paging.PagingResponse;
import com.example.genieapp.dto.request.artist.ArtistFormRequest;
import com.example.genieapp.dto.response.artist.ArtistFormResponse;

public interface ArtistService {

    void create(ArtistFormRequest artistFormRequest);

    ArtistFormResponse read(Long id);

    void modify(Long id, ArtistFormRequest artistFormRequest);

    void remove(Long id);

    PagingResponse<ArtistFormResponse> search(String searchWord, PagingRequest pagingRequest);
}
